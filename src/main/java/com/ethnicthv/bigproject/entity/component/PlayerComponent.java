package com.ethnicthv.bigproject.entity.component;

import com.almasb.fxgl.core.serialization.Bundle;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.entity.component.SerializableComponent;
import org.jetbrains.annotations.NotNull;

public class PlayerComponent extends Component implements SerializableComponent {
    @Override
    public void read(@NotNull Bundle bundle) {
    }

    @Override
    public void write(@NotNull Bundle bundle) {

    }
}
